//Addison Mincham 16003689 Assignment 1
package uni16003689.addison.assignment1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_NUMBER = 1;

    //Making a type TextView for the display of numbers
    TextView numberview;

    //String that will hold the users number they will type
    String callnumber = "";

    //Variables of type Button for each of the buttons the user will be pressing in the app
    Button buttonOne, buttonTwo, buttonThree, buttonFour, buttonFive, buttonSix, buttonSeven, buttonEight, buttonNine, buttonAlt, buttonZero, buttonHash, buttonBack, buttonCall,buttonEmergency,buttonVoiceMail;

    //Function to save the state of the app, used primarily when rotating the device from portrait to horizontal and vice versa.
    @Override
    public void onSaveInstanceState(Bundle savedState){
        super.onSaveInstanceState(savedState);
        savedState.putString("Number",callnumber);
    }

    //Restoring the saved state and text view from the saved state
    @Override
    public void onRestoreInstanceState(Bundle savedState){
        super.onRestoreInstanceState(savedState);
        callnumber = savedState.getString("Number");
        numberview.setText(callnumber);

    }
    //Main piece of the app when onCreate of the app (when the user starts it up) it will run this.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the layout for what the user will see in the app
        setContentView(R.layout.activity_main);

        //Permissions to check if the app has the permission to use the CALL_PHONE feature, which this app relies on
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.CALL_PHONE}, PERMISSION_NUMBER);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_NUMBER);
            }
        }

        //setting the textView variable to the actually textView in the layout
        numberview = (TextView) findViewById(R.id.editText);

        //Setting all the variable buttons to the actual buttons on the layout.
        buttonOne = (Button) findViewById(R.id.buttonOne);
        buttonTwo = (Button) findViewById(R.id.buttonTwo);
        buttonThree = (Button) findViewById(R.id.buttonThree);
        buttonFour = (Button) findViewById(R.id.buttonFour);
        buttonFive = (Button) findViewById(R.id.buttonFive);
        buttonSix = (Button) findViewById(R.id.buttonSix);
        buttonSeven = (Button) findViewById(R.id.buttonSeven);
        buttonEight = (Button) findViewById(R.id.buttonEight);
        buttonNine = (Button) findViewById(R.id.buttonNine);
        buttonAlt = (Button) findViewById(R.id.buttonAlt);
        buttonZero = (Button) findViewById(R.id.buttonZero);
        buttonHash = (Button) findViewById(R.id.buttonHash);
        buttonCall = (Button) findViewById(R.id.buttonCall);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonEmergency = (Button) findViewById(R.id.emergency);
        buttonVoiceMail = (Button) findViewById(R.id.voiceMail);


        //This all sets the Listeners for each Button and to either update the call number and updates the text on the screen, or calls the specified number.
        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "1";
                numberview.setText(callnumber);
            }
        });

        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "2";
                numberview.setText(callnumber);
            }
        });

        buttonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "3";
                numberview.setText(callnumber);
            }
        });

        buttonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "4";
                numberview.setText(callnumber);
            }
        });

        buttonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "5";
                numberview.setText(callnumber);
            }
        });

        buttonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "6";
                numberview.setText(callnumber);
            }
        });

        buttonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "7";
                numberview.setText(callnumber);
            }
        });

        buttonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "8";
                numberview.setText(callnumber);
            }
        });

        buttonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "9";
                numberview.setText(callnumber);
            }
        });

        buttonAlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "*";
                numberview.setText(callnumber);
            }
        });

        buttonZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "0";
                numberview.setText(callnumber);
            }
        });

        buttonHash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callnumber = callnumber + "#";
                numberview.setText(callnumber);
            }
        });

        //Specifically if the user clicks the call button it will parse the number/string given from the user to the intent to then call that number.
        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + callnumber));
                startActivity(intent);
            }
        });

        //This is the same with these next two, but instead of the user passing the specified number, it is hard coded here.
        buttonEmergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + "111"));
                startActivity(intent);
            }
        });
        buttonVoiceMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + "707"));
                startActivity(intent);
            }
        });


        //Button to delete the previous number int he textView, Unless it has nothing in the textView where it will just kep it as nothing in the textView.
        //This is to make sure the app doesn't crash if there is nothing in the textView and the user still presses this button.
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callnumber.equals("")) {
                    callnumber = "";
                } else {
                    callnumber = callnumber.substring(0, callnumber.length() - 1);
                    numberview.setText(callnumber);
                }
            }
        });
    }

    //Function when the user gives the result on whether they accept or deny the permission, it will show a message at the bottom of the screen for wither result.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_NUMBER:{
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE)==PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Yay, Permission Granted!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this, "Oh No, Permission Denied", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }
    }
}
